## [1.0.4](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/compare/v1.0.3...v1.0.4) (2021-09-11)


### Bug Fixes

* **nuget:** fixing packaging and versioning ([a6fe769](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/commit/a6fe769feb307c1d85534b603d045bd9fbb37778))
* **nuget:** fixing packaging and versioning ([1e8dd7a](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/commit/1e8dd7a9a9d69d13173e513ab8fcb3116bd04f8b))
* **nuget:** fixing packaging and versioning ([4941485](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/commit/4941485989daedf64e9ff73493305a71e7023c6e))

## [1.0.3](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/compare/v1.0.2...v1.0.3) (2021-09-10)


### Bug Fixes

* test output should go to the Xunit output ([d7902d2](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/commit/d7902d215aba596a42b7c2e11e3483bb4067e674))

## [1.0.2](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/compare/v1.0.1...v1.0.2) (2021-09-10)


### Bug Fixes

* **nuget:** adding description ([40745c5](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/commit/40745c524f062982eeb75854b47ff08191446fa5))

## [1.0.1](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/compare/v1.0.0...v1.0.1) (2021-09-10)


### Bug Fixes

* correcting NuGet packaging ([87cf2f9](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/commit/87cf2f91c4ab2a499f1d1ddcfd5590fe66e3f9ae))

# 1.0.0 (2021-09-10)


### Features

* initial commit ([abecc1f](https://gitlab.com/mfgames-cil/mfgames-testsetup-cil/commit/abecc1f34cfb0efcc6d30c59ae00f781566ab4c3))
