using Serilog;
using Serilog.Core;
using Serilog.Events;
using Serilog.Exceptions;

using Xunit.Abstractions;

namespace MfGames.TestSetup
{
    /// <summary>
    /// Sets up a test that uses Serilog for logging, Autofac as container
    /// resolution, and other common configuration elements.
    /// </summary>
    public abstract class TestBase<TContext>
        where TContext : TestContext, new()
    {
        protected TestBase(ITestOutputHelper output)
        {
            // Set up the logging output.
            this.Output = output;
            this.Logger = new LoggerConfiguration()
                .Enrich.WithDemystifiedStackTraces()
                .Enrich.WithExceptionDetails()
                .Enrich.FromLogContext()
                .MinimumLevel.Is(LogEventLevel.Debug)
                .WriteTo.TestOutput(
                    output,
                    outputTemplate:
                    "[{Level:u3}] {Message}{NewLine}{Exception}")
                .CreateLogger();
        }

        protected Logger Logger { get; }

        protected ITestOutputHelper Output { get; }

        protected virtual TContext CreateContext()
        {
            var context = new TContext();

            context.SetLogger(this.Logger);
            context.ConfigureContainer();

            return context;
        }
    }
}
