using System;

using Autofac;

using Serilog;

namespace MfGames.TestSetup
{
    /// <summary>
    /// A context for the test run that includes the container.
    /// </summary>
    public class TestContext : IDisposable
    {
        private IContainer? container;

        private ILogger? logger;

        public IContainer Container
        {
            get => this.container
                ?? throw new NullReferenceException(
                    "ConfigureContainer has not been called.");
            private set => this.container = value;
        }

        public ILogger Logger
        {
            get => this.logger
                ?? throw new NullReferenceException(
                    "SetLogger has not been called.");
            private set => this.logger = value;
        }

        public void ConfigureContainer()
        {
            var builder = new ContainerBuilder();

            builder
                .RegisterInstance(this.Logger)
                .As<ILogger>()
                .SingleInstance();

            this.ConfigureContainer(builder);

            this.Container = builder.Build();
        }

        /// <inheritdoc />
        public void Dispose()
        {
            this.container?.Dispose();
        }

        public TType Resolve<TType>()
            where TType : class
        {
            return this.Container.Resolve<TType>();
        }

        public void SetLogger(ILogger value)
        {
            this.Logger = value;
        }

        protected virtual void ConfigureContainer(ContainerBuilder builder)
        {
        }
    }
}
